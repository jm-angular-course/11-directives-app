import {
  Directive,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[appCustomIf]',
})
export class CustomIfDirective implements OnInit, OnDestroy {
  private readonly CLASS_NAME = CustomIfDirective.name;
  @Input() set appCustomIf(value: boolean) {
    if (value) {
      this.viewContainer.createEmbeddedView(this.template);
    } else {
      this.viewContainer.clear();
    }
  }

  constructor(
    private template: TemplateRef<HTMLElement>,
    private viewContainer: ViewContainerRef
  ) {
    console.log(`${this.CLASS_NAME} - constructor`);
    console.log(template);
  }

  ngOnInit(): void {
    console.log(`${this.CLASS_NAME} - ngOnInit`);
  }

  ngOnDestroy(): void {
    console.log(`${this.CLASS_NAME} - ngOnDestroy`);
  }
}
