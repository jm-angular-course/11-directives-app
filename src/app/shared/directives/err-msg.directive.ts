import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appErrMsg]',
})
export class ErrMsgDirective implements OnInit {
  private _color: string = 'red';
  private _msg: string = 'El campo es requerido';

  @Input() set msg(msg: string) {
    this._msg = msg;
    this.setMsg();
  }

  @Input() set color(color: string) {
    this._color = color;
    this.setColor();
  }

  @Input() set valid(valid: boolean) {
    if (valid) {
      this.htmlElement.classList.add('hidden');
    } else {
      this.htmlElement.classList.remove('hidden');
    }
  }

  htmlElement: HTMLElement;

  constructor(private el: ElementRef<HTMLElement>) {
    this.htmlElement = this.el.nativeElement;
  }

  ngOnInit(): void {
    this.setMsg();
    this.setColor();
    this.setClass();
  }

  setMsg(): void {
    this.htmlElement.innerText = this._msg;
  }

  setColor(): void {
    this.htmlElement.style.color = this._color;
  }

  setClass(): void {
    this.htmlElement.classList.add('form-text');
  }
}
