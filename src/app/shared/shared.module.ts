import { NgModule } from '@angular/core';
import { CustomIfDirective } from './directives/custom-if.directive';
import { ErrMsgDirective } from './directives/err-msg.directive';

@NgModule({
  declarations: [ErrMsgDirective, CustomIfDirective],
  exports: [ErrMsgDirective, CustomIfDirective],
})
export class SharedModule {}
