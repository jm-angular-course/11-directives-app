import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [],
})
export class AddComponent {
  private readonly NAMES = [
    'Jesús Mendoza',
    'Georgina Navarro',
    'Jesús Mendoza Jr.',
    'Jeampool Mendoza',
  ];
  myForm: FormGroup = this.fb.group({ name: ['', Validators.required] });
  text: string = 'Jeanpier Mendoza';
  color: string = 'green';

  constructor(private fb: FormBuilder) {}

  hasError(field: string): boolean {
    return this.myForm.get(field)?.invalid || false;
  }

  changeName() {
    const name = this.NAMES[this.getRandomRounded(this.NAMES.length)];
    this.text = name;
  }

  changeColor() {
    const color = '#xxxxxx'.replace(/x/g, (y) =>
      this.getRandomRounded(16).toString(16)
    );
    this.color = color;
  }

  private getRandomRounded(max: number = 1) {
    return (Math.random() * max) | 0;
  }
}
